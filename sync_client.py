import requests

URL_TEMPLATE = "https://api.icndb.com/jokes/{id}/"


def get_joke(id: int) -> str:
    """
    Get a joke with the given id from The Internet Chuck Norris Database

    Args:
        id: The joke id

    Returns:
        The joke with the given id from the The Internet Chuck Norris Database

    Raises:
        JokeApiError
    """
    try:

        response = requests.get(URL_TEMPLATE.format(id=id))

        if response.status_code != 200:
            raise JokeApiError("API request failed")

        data = response.json()
        if data.get("type") == 'NoSuchQuoteException':
            raise NoSuchJoke(data.get("value", ""))
        if data.get("type") != "success":
            raise JokeApiError("API request failed")

        return data["value"]["joke"]

    except JokeApiError:
        raise
    except Exception as e:
        raise JokeApiError() from e


class JokeApiError(Exception):
    pass


class NoSuchJoke(JokeApiError):
    pass
